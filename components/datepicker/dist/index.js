"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _antd = require("antd");

var _moment = _interopRequireDefault(require("moment"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireDefault(require("react"));

require("./style.scss");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var popupStyle = {
  color: "#FF0000"
};
var style = {
  boxShadow: "none",
  width: "100%",
  marginTop: 8,
  borderRadius: 8,
  paddingTop: 10,
  paddingBottom: 10
};

var DatePicker = function DatePicker(_ref) {
  var placeholder = _ref.placeholder,
      errors = _ref.errors,
      touched = _ref.touched,
      _onChange = _ref.onChange,
      allowClear = _ref.allowClear,
      value = _ref.value,
      disabledDate = _ref.disabledDate,
      className = _ref.className,
      hidePlaceholder = _ref.hidePlaceholder,
      rest = _objectWithoutProperties(_ref, ["placeholder", "errors", "touched", "onChange", "allowClear", "value", "disabledDate", "className", "hidePlaceholder"]);

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
    className: "input-wrapper space-between",
    children: [!hidePlaceholder && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: "date-placeholder",
      children: placeholder
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_antd.DatePicker, _objectSpread({
      placeholder: placeholder,
      className: ["".concat(touched && errors && "date-picker-err"), "date-picker-custom", className],
      popupStyle: popupStyle,
      style: style,
      allowClear: allowClear,
      value: value && (0, _moment["default"])(value),
      suffixIcon: /*#__PURE__*/(0, _jsxRuntime.jsx)(_jsxRuntime.Fragment, {}),
      onChange: function onChange(date) {
        return _onChange(date.format("YYYY-MM-DD"));
      },
      size: "large",
      disabledDate: disabledDate
    }, rest)), touched && /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
      className: "err",
      children: errors
    })]
  });
};

DatePicker.defaultProps = {
  allowClear: true,
  hidePlaceholder: false,
  onChange: function onChange() {}
};
DatePicker.propTypes = {
  allowClear: _propTypes["default"].bool,
  onChange: _propTypes["default"].func,
  disabledDate: _propTypes["default"].func,
  placeholder: _propTypes["default"].string,
  errors: _propTypes["default"].string,
  touched: _propTypes["default"].bool,
  value: _propTypes["default"].string,
  className: _propTypes["default"].string,
  hidePlaceholder: _propTypes["default"].bool
};
var _default = DatePicker;
exports["default"] = _default;

//# sourceMappingURL=index.js.map