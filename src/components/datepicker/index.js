import { DatePicker as AntdDatePicker } from "antd";
import moment from "moment";
import propTypes from "prop-types";
import React from "react";
import "./style.css";

const popupStyle = {
  color: "#FF0000",
};
const style = {
  boxShadow: "none",
  width: "100%",
  marginTop: 8,
  borderRadius: 8,
  paddingTop: 10,
  paddingBottom: 10,
};
const DatePicker = ({
  placeholder,
  errors,
  touched,
  onChange,
  allowClear,
  value,
  disabledDate,
  className,
  hidePlaceholder,
  ...rest
}) => {
  return (
    <div className="input-wrapper space-between">
      {!hidePlaceholder && <p className={"date-placeholder"}>{placeholder}</p>}
      <AntdDatePicker
        placeholder={placeholder}
        className={[
          `${touched && errors && "date-picker-err"}`,
          "date-picker-custom",
          className,
        ]}
        popupStyle={popupStyle}
        style={style}
        allowClear={allowClear}
        value={value && moment(value)}
        suffixIcon={<></>}
        onChange={date => onChange(date.format("YYYY-MM-DD"))}
        size={"large"}
        disabledDate={disabledDate}
        {...rest}
      />
      {touched && <p className={"err"}>{errors}</p>}
    </div>
  );
};

DatePicker.defaultProps = {
  allowClear: true,
  hidePlaceholder: false,
  onChange: () => {},
};

DatePicker.propTypes = {
  allowClear: propTypes.bool,
  onChange: propTypes.func,
  disabledDate: propTypes.func,
  placeholder: propTypes.string,
  errors: propTypes.string,
  touched: propTypes.bool,
  value: propTypes.string,
  className: propTypes.string,
  hidePlaceholder: propTypes.bool,
};

export default DatePicker;
